// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAH3ImhcanExXlySVF6_OA7c6Lcue9c0vQ",
  authDomain: "fir-app-batch-may-22.firebaseapp.com",
  projectId: "fir-app-batch-may-22",
  storageBucket: "fir-app-batch-may-22.appspot.com",
  messagingSenderId: "508882370713",
  appId: "1:508882370713:web:6fefb578a329fd2f177808"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
export const db = getFirestore(app);
// Initialize Cloud Storage and get a reference to the service
export const storage = getStorage(app);