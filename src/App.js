import { async } from '@firebase/util';
import { collection, getDocs, onSnapshot } from 'firebase/firestore';
import { deleteObject, getDownloadURL, ref, uploadBytes } from 'firebase/storage';
import { useEffect, useState } from 'react';
import { db, storage } from './firebase';

function App() {
  const [ products, setProducts ] = useState([]);
  const [ uploadImage, setUploadImage ] = useState(null);

  // untuk collections
  const productCollections = collection(db, "products");

  useEffect(() => {

    // GET DATA TIDAK REALTIME
    const unsubscribe = async () => {
      try {
        const data = [];
        const querySnapshot = await getDocs(productCollections);
        querySnapshot.forEach((doc) => {
          data.push(doc.data());
        })
        setProducts(data);
      } catch (err) {
        console.log(err);
      }
    }

    // // manggil function 
    unsubscribe();

    // UNTUK REALTIME FIRESTORE / onSnapshot
    // nge watch/waiting
    // const unsubscribe = onSnapshot(productCollections, (snapshot) => {
    //   const data = [];
    //   snapshot.docs.map(doc => {
    //     data.push(doc.data());
    //   })

    //   setProducts(data);
    // })

    // unmounted
    return() => unsubscribe();
  }, [])

  
  const upload = (event) => {
    // untuk storage
    let fileName = event.target.files[0].name;
    const storageRef = ref(storage, `images/${fileName}`);
    
    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, event.target.files[0])
    .then(snapshot => {
      console.log("Upload berhasil");
      console.log(snapshot);
    })
    .catch(err => {
      console.log(err);
    })
    .finally(() => {
      getDownloadURL(storageRef)
      .then(downloadUrl => {
        // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
        setUploadImage(downloadUrl);
        console.log(downloadUrl);
      })
    })
  }

  // ini ngga jalan, hanya contoh logika
  const edit = () => {
    // dapat url baru
    // url yang lama teman-teman simpan dulu nih di variable lokal
    // let prevImage = book.cover
    fetch(`url://api`, { method: "PUT", body: ....})
    .then(response => {
      if (response.ok) {
        const storageRef = ref(storage, `images/${prevImage}`);
        // disinilah teman-teman menghapus image sebelumnya!
        deleteObject(storageRef)
      } else { // kalo terjadi error
        // image baru yang disimpan itu harus di delete karna data tidak terupdate di server!
      }
    })
  }

  return (
    <div >
      {products.map((product,i) => (
        <div key={i}>
          <h2>{product.title}</h2>
        </div>
      ))}

      <div>
        <input type="file" placeholder="upload image" onChange={upload}/>
      </div>
      <img src={uploadImage}/>
    </div>
  );
}

export default App;
